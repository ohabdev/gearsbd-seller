import { Component, OnInit, Renderer } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services';
import { LocationService } from '../../shared/services';
import { ToastyService } from 'ng2-toasty';

@Component({
  styleUrls: ['register.css'],
  templateUrl: 'register.html'
})
export class RegisterComponent implements OnInit {
  public withTradeLicense: boolean = true; // Manage Registration Type. With Trade License or Without Trade License.
  public dialCode: any = '';
  public shop: any = {
    email: '',
    password: ''
  };
  public confirmPassword: string = '';

  public issueDocumentOptions: any;
  // public issueDocument: any;
  public issueDocument: any ;
  public submitted: boolean = false;
  public logoUrl: any;
  public currentUser: any;
  public firstStepDone:boolean = false;
  public registrationDone:boolean = false;
  public formData: any = {};
  public countries: any = [];
  public districts: any = [];
  private registrationType: any  = 1;

  // Documents

  public tradeLicenseDocuments: any;
  public tradeLicenseDocumentsOptions: any;

  public nidDocuments: any;
  public nidDocumentsOptions: any;

  public proofDocuments: any;
  public proofDocumentsOptions: any;

  public uploadedDocuments: any = [];

  constructor(
    private auth: AuthService,
    public router: Router,
    private route: ActivatedRoute,
    private toasty: ToastyService,
    private render: Renderer,
    private location: LocationService
    ) {
    this.logoUrl = route.snapshot.data['appConfig'] ? route.snapshot.data['appConfig'].siteLogo : '/assets/images/logo.jpg';

    // Set Default Valu

    this.shop = {
      "name": "",
      "firstName": "",
      "lastName": "",
      "username": "",
      "email": "",
      "phoneNumber": "",
      "officeAddress": "",
      "officeCity": "",
      "officeZip": "",
      "address": "",
      "city": "",
      "country": "",
      "tradeLicenseNumber": "",
      "nidno": ""
    }
  }

  ngOnInit() {
    if (this.auth.isLoggedin()) {
      this.auth.getCurrentUser().then(user => {
        this.currentUser = user;
      });
    }

    this.location.countries().then(response => {
      this.countries = response.data;
      var bangladesh = this.countries.find(country => country.isoCode == 'BD');
      this.shop.country = bangladesh._id;
    });

    this.location.districts().then(response => {
      this.districts = response.data;
      // console.log(this.districts);
    });
    // TODO - check if user login here or the link have access token
    // then we can query user and hide password field and show user info
    // this.issueDocumentOptions = {
    //   url: window.appConfig.apiBaseUrl + '/shops/register/document',
    //   fileFieldName: 'file',
    //   hintText: 'Click or drag verificaiton document here',
    //   multiple: true,
    //   onFinish: (resp) => {
    //     this.issueDocument = [];
    //     resp.forEach(data => {
    //       this.issueDocument.push(data.data);
    //     });
    //     console.log(this.issueDocument);
    //   }
    // };
    
    this.uploadedDocuments['tradelicense'] = [];
    this.uploadedDocuments['nid'] = [];
    this.uploadedDocuments['proof'] = [];
    
    this.tradeLicenseDocumentsOptions = {
      url: window.appConfig.apiBaseUrl + '/shops/register/document',
      fileFieldName: 'file',
      hintText: 'Click or drag first 3 page(s) scan copies of the license',
      multiple: true,
      docType: 'tradelicense',
      docCount: ['page1', 'page2', 'page3'],
      onFileSelect: (data)=>{
        console.log('data',data)
      },
      onFinish: (resp) => {        
        this.tradeLicenseDocuments = resp['tradelicense'];
        // this.uploadedDocuments['tradelicense'] = this.tradeLicenseDocuments.data._id;
        // this.uploadedDocuments['tradelicense'].push(this.tradeLicenseDocuments.data._id);
        this.tradeLicenseDocuments.forEach(data => {
          console.log("data", data);
          this.uploadedDocuments['tradelicense'].push(data.data._id);
        });
        // console.log(this.uploadedDocuments['tradelicense'][0]);
      }
    };
    console.log('this.tradeLicenseDocumentsOptions', this.tradeLicenseDocumentsOptions);
    
    this.nidDocumentsOptions = {
      url: window.appConfig.apiBaseUrl + '/shops/register/document',
      fileFieldName: 'file',
      hintText: 'Click or drag front and back scan copies of NID',
      multiple: true,
      docType: 'nid',
      docCount: ['front', 'back'],
      onFinish: (resp) => {
        this.nidDocuments = resp['nid'];
        // this.uploadedDocuments['nid'].push(this.nidDocuments.data._id);
        // console.log(this.uploadedDocuments);
        this.nidDocuments.forEach(data => {
          this.uploadedDocuments['nid'].push(data.data._id);
        });
        console.log(this.uploadedDocuments);
      }
    };

    this.proofDocumentsOptions = {
      url: window.appConfig.apiBaseUrl + '/shops/register/document',
      fileFieldName: 'file',
      hintText: 'Click or drag Photo Proof of your documents',
      // uploadOnSelect: true,
      // autoUpload: true,
      docType: 'proof',
      docCount: ['proof'],
      onFinish: (resp) => {
        // console.log(resp);
        this.proofDocuments = resp['proof'];
        this.uploadedDocuments['proof'].push(this.proofDocuments.data._id);
        // // console.log(this.uploadedDocuments);
        // this.proofDocuments.forEach(data => {
        //   this.uploadedDocuments['proof'].push(data.data._id);
        // });
        console.log(this.uploadedDocuments);

      }
    };
  }

  public submit(form: any) {
    this.issueDocument = this.uploadedDocuments;
    this.submitted = true;

    // console.log(this.issueDocument);

    if (form.invalid) {
      return;
    }

    // if (this.shop.password !== this.confirmPassword) {
    //   return this.toasty.error('Password does not match.');
    // }

    // if (!this.issueDocument) {
    //   return this.toasty.error('Please upload document for verification.');
    // }

    // Prepare Data
    // this.shop.name = `${this.shop.firstName} ${this.shop.lastName}`;
    this.shop.phoneNumber = `${this.dialCode}${this.shop.phoneNumber}`;
    // this.shop.verificationIssueId = this.issueDocument._id;
    this.shop.scannedCopyOfTL = this.uploadedDocuments['tradelicense'][0]; // Sending Single Doc
    this.shop.nidFront = this.uploadedDocuments['nid'][0];
    this.shop.nidBack = this.uploadedDocuments['nid'][1];
    this.shop.proof = this.uploadedDocuments['proof'][0];

    // Prepare Form Data

    let businessInfo = {
      name: this.shop.name,
      identifier: this.shop.tradeLicenseNumber,
      address: this.shop.officeAddress + "," + this.shop.officeCity + "," + this.shop.officeZip,  
    }

    this.formData = {
      "name": this.shop.name,
      "email": this.shop.email,
      "username": this.shop.username,
      "phoneNumber": "+88" + this.shop.phoneNumber,
      "personalBkashNumber": "+88" + this.shop.personalBkashNumber,
      "password": this.shop.password,
      "address": this.shop.address,
      "zipcode": this.shop.officeZip.toString(),
      "city": this.shop.city,
      // "state": this.shop.state,
      "country": this.shop.country,
      "tradeLicenseNumber": this.shop.tradeLicenseNumber,
      "nidno": this.shop.nidno,
      // "scannedCopyOfTL": this.shop.scannedCopyOfTL,
      // "officeAddress": this.shop.officeAddress,
      // "officeCity": this.shop.officeCity,
      // "officeZip": this.shop.officeZip,
      "businessInfo": businessInfo,
      "nidFront": this.shop.nidFront,
      "nidBack" : this.shop.nidBack,
      "proof": this.shop.proof,
      "type": this.registrationType
    }
    console.log(this.formData);
    
    // return 
    if(!this.withTradeLicense){
      this.formData.tradeLicenseNumber  = '';
      this.formData.scannedCopyOfTL     = '';

    }
    else{
      this.formData.tradeLicenseNumber  = this.shop.tradeLicenseNumber;
      // this.formData.scannedCopyOfTL     = this.uploadedDocuments['tradelicense'][0];
      this.formData.scannedCopyOfTL     = JSON.stringify(this.uploadedDocuments['tradelicense']);
    }

    // console.log(this.formData);
    // this.registrationDone = true;
    // return;
    console.log(this.formData);
    this.auth.register(this.formData)
      .then(resp => {
        this.registrationDone = true;
        this.toasty.success('Your account has been created. Wait admin to verify.');
        // this.router.navigate(['/auth/login']); 
      })
      .catch(e => this.toasty.error(e.data.data.message || 'As a seller unable to register from seller part')); // TODO - implement me 
  }

  public selectDial(event) {
    this.dialCode = event;
  }
 

  nextStep(id){

    if (this.shop.password !== this.confirmPassword) {
      return this.toasty.error('Password does not match.');
    }

    if(this.withTradeLicense){
      if(this.shop.name && this.shop.username && this.shop.email &&  this.shop.phoneNumber && this.shop.personalBkashNumber && this.shop.tradeLicenseNumber && this.uploadedDocuments['tradelicense'].length>2 && this.uploadedDocuments['nid'].length>1 && this.uploadedDocuments['proof'].length>0){
        this.firstStepDone = true;
      }
      else if(!this.shop.name){
        return this.toasty.error('Name is Required');
      }
      else if(!this.shop.username){
        return this.toasty.error('Username is Required');
      }
      else if(!this.shop.email){
        return this.toasty.error('Email is Required');
      }
      else if(!this.shop.phoneNumber){
        return this.toasty.error('Phone Number is Required');
      }
      else if(!this.shop.personalBkashNumber){
        return this.toasty.error('Bkash Number is Required');
      }
      else if(!this.shop.tradeLicenseNumber){
        return this.toasty.error('Trade LicenseN Number is Required');
      }
      else if(this.uploadedDocuments['tradelicense'].length === 0){
        return this.toasty.error('Trade License not uploaded!');
      }
      else if(this.uploadedDocuments['nid'].length === 0){
        return this.toasty.error('NID not uploaded!');
      }
      else if(this.uploadedDocuments['proof'].length === 0){
        return this.toasty.error('Photo Proof not uploaded!');
      }
       
    }
    else{
      if(this.shop.name && this.shop.username && this.shop.email &&  this.shop.phoneNumber && this.shop.personalBkashNumber  && this.uploadedDocuments['nid'].length>1 && this.uploadedDocuments['proof'].length>0){
        this.firstStepDone = true;
      }
      else if(!this.shop.name){
        return this.toasty.error('Name is Required');
      }
      else if(!this.shop.username){
        return this.toasty.error('Username is Required');
      }
      else if(!this.shop.email){
        return this.toasty.error('Email is Required');
      }
      else if(!this.shop.phoneNumber){
        return this.toasty.error('Phone Number is Required');
      }
      else if(!this.shop.personalBkashNumber){
        return this.toasty.error('Bkash Number is Required');
      }
      else if(this.uploadedDocuments['nid'].length === 0){
        return this.toasty.error('NID not uploaded!');
      }
      else if(this.uploadedDocuments['proof'].length === 0){
        return this.toasty.error('Photo Proof not uploaded!');
      }
    }


  }

  onlyNumber(event) {
    return (event.charCode === 8 || event.charCode === 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }
  goPreviousStep(){
    this.firstStepDone = false;
  }
  // Manage SignUp Type

  withNID(event){
    event.preventDefault();
    if(this.firstStepDone){
      return;
    }
    this.withTradeLicense = false;
    this.registrationType = 2;
  }

  withtradeLicense(event){
    event.preventDefault();
    if(this.firstStepDone){
      return;
    }
    this.withTradeLicense = true;
    this.registrationType = 1;
    // }
  }

  backtoLogin(){
    this.router.navigate(['/auth/login']);
  }
}
