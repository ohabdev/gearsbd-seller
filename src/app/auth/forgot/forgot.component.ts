import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services';
import { ToastyService } from 'ng2-toasty';

@Component({
  templateUrl: 'forgot.html',
  styleUrls: ['forgot.css']
})
export class ForgotComponent {
	public email: string = '';
	public submitted: boolean = false;
	private Auth: AuthService;
	public logoUrl: any;

	constructor(auth: AuthService, public router: Router, private route: ActivatedRoute, private toasty: ToastyService) {
    this.logoUrl = route.snapshot.data['appConfig'] ? route.snapshot.data['appConfig'].siteLogo : '/assets/images/logo.jpg';
    // this.logoUrl = '/assets/images/logo.png';
		this.Auth = auth;
	}

	forgot(frm: any) {
		this.submitted = true;
		this.Auth.forgot(this.email).then((resp) => {
      console.log(resp);
			this.toasty.success('New password has been sent, please check your email inbox.');
			this.email = '';
			this.submitted = false;
			frm.reset();
		})
    .catch((err) => {
      // console.log(err);
      if(err.data.data.message){
        return this.toasty.error(err.data.data.message);
      }
      else if (err.data.data.details[0]) {
        return this.toasty.error(err.data.data.details[0].message);
      }
      return this.toasty.error('Something went wrong, please try again!');
    });
	}
}
