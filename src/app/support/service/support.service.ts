import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  private allowFields = [
    'subject', 'category', 'reference', 'priority', 'description'
  ];

  private replyAllowFields = [
    'ticketId', 'attachment', 'description'
  ];

  constructor(private restangular: Restangular) { }

  find(params: any): Promise<any> {
    return this.restangular.one('tickets').get(params).toPromise();
  }

  findOne(id): Promise<any> {
    return this.restangular.one('tickets', id).one('comments').get().toPromise();
  }

  create(data: any): Promise<any> {
    return this.restangular.all('tickets').post(_.pick(data, this.allowFields)).toPromise();
  }

  addReply(id, data: any): Promise<any> {
    return this.restangular.one('tickets', id).all('comments').post(_.pick(data, this.replyAllowFields)).toPromise();
    // return this.restangular.all('tickets', id).one('comments').post(data).toPromise();
  }

  updateStatus(id, data): Promise<any> {
    // console.log(data);
    return this.restangular.one('tickets', id).one('status').customPUT(data).toPromise();
  }
}
