import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../service/support.service';
import { AuthService } from '../../../shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';
import * as moment from 'moment-timezone';

@Component({
  selector: 'support-list',
  templateUrl: './list.html',
})
export class ListComponent implements OnInit {
  public tickets = [];

  public page: Number = 1;
  public take: Number = 10;
  public total: Number = 0;
  public sortOption: any = {
    sortBy: 'createdAt',
    sortType: 'desc'
  };

  constructor(private authService: AuthService, private router: Router, private supportService: SupportService, private toasty: ToastyService) { }

  ngOnInit() {
    this.query();
  }

  query(){
    const params = Object.assign({
      page: this.page,
      take: this.take,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`,
    });

    this.supportService.find(params).then((res) => {
      this.tickets = res.data.items;
      this.total = res.data.count;
      // console.log(this.tickets);
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));
  }

}
