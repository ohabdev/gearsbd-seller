import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { ProductService } from '../../../../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'request-product',
  templateUrl: './requestproduct.html',
  styleUrls: ['./requestproduct.css']
})
export class RequestproductComponent implements OnInit {

  public product : any = {
    name : '',
    description: ''
    // productType: 'digital'
  };
  public isSubmitted: any = false;
  constructor(private toasty: ToastyService, private productService: ProductService, private router: Router) { }

  ngOnInit() {
  }
  submit(frm: any) {
    this.isSubmitted = true;
    if (frm.invalid) {
      return this.toasty.error('Form is invalid, please try again.');
    }

    this.productService.request(this.product)
      .then(() => {
        this.toasty.success('Product request has been created');
        this.router.navigate(['/products/request/list']);
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }

}
