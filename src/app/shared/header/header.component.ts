import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../services';
import { UserService } from './../../user/user.service';
import { Router } from '@angular/router';
import { RestangularModule, Restangular } from 'ngx-restangular';
import {ToastyService} from 'ng2-toasty';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  // @Input() user: any;
  public user: any = {};
  public avatarUrl = '';
  public notifications = {total: 0, data: [], page: 0, loading: false, notificationHas: false};
  public avatarOptions: any = {};
  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private restangular: Restangular,
    private toasty: ToastyService,
  ) { }

  ngOnInit() {
    this.findNotificationQuery();
    this.userService.me().then(resp => {
      this.user = resp.data;
      // console.log(this.user);
      this.avatarUrl = resp.data.avatarUrl;
    });

  }

  findNotificationQuery() {
    this.notifications = { ...this.notifications, loading: true };
    this.restangular.one('notifications/seller?page='+ (this.notifications.page + 1)).get().toPromise().then((res)=>{
      const data = { total: res.data.total, data: [...this.notifications.data, ...res.data.notifications], page: parseInt(res.data.page), loading: false, notificationHas: false };
      if(res.data.notifications.length >= res.data.per_page){
        data.notificationHas = true
      }
      this.notifications = data;
    }) .catch(() => this.toasty.error('Something went wrong, please try again!'));
  }


  seeMore(event){
    event.preventDefault();
    event.stopPropagation();
    this.notifications.loading = true;
    this.findNotificationQuery()
  }

  logout() {
    this.authService.removeToken();
    this.router.navigate(['/auth/login']);
  }

}
