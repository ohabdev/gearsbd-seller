import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SettlementRoutingModule } from './settlement.routing';
import { ListingComponent } from './components/listing/listing.component';
import { ViewComponent } from './components/view/view.component';
import { SettlementService } from './service/settlement.service';
import { StatService } from '../shared/services';
import { WithdrawFormComponent } from './withdraw-form/withdraw-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SettlementRoutingModule,
    NgbModule.forRoot(), 
    FormsModule
  ],
  declarations: [
    ListingComponent, 
    ViewComponent, 
    WithdrawFormComponent
  ],
  providers: [
    SettlementService,
    StatService
  ],
  exports: [], 
  entryComponents: [WithdrawFormComponent]
})
export class SettlementModule { }
