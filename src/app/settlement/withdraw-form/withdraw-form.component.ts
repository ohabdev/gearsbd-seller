import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettlementService } from './../service/settlement.service';
import { ToastyService } from 'ng2-toasty';

import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { SettlementService } from "./../../service/settlement.service";

@Component({
  selector: 'withdraw-form',
  templateUrl: './withdraw-form.html'
})
export class WithdrawFormComponent implements OnInit {

  public amount: Number = 0;
  public param =  {
    'amount' : 0,
  };

  constructor(public activeModal: NgbActiveModal, private withdrawService: SettlementService, private toasty: ToastyService) { }

  ngOnInit() {
    this.withdrawService.getBalance().then(resp => {
      // console.log(resp.data);
      this.param.amount = resp.data.balance;
      this.amount       = resp.data.balance
    })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));
  }
  submit() {

    if(this.param.amount < 1){
      return this.toasty.error('Amount must be greater than 0!');
    }

    if(this.param.amount > this.amount){
      return this.toasty.error('Amount must be less than or equal ' + this.amount + '!');
    }
    
    this.withdrawService.payOutRequest(this.param).then((res) => {
      this.toasty.success('Request sent!');
      location.reload();
    }).catch((err) => {
      console.log(err);
      this.toasty.error('Something went wrong, please check and try again!');
    })
    this.activeModal.close();
  }
}