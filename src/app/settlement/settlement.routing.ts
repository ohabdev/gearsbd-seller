import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './components/listing/listing.component';
import { ViewComponent } from './components/view/view.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListingComponent,
    data: {
      title: 'Settlement manager',
      urls: [{ title: 'Settlements', url: '/settelements/list' }]
    }
  },
  {
    path: 'view/:id',
    component: ViewComponent,
    data: {
      title: 'Settlement manager',
      urls: [{ title: 'Settlements ', url: '/settelements/list' }, { title: 'Detail', url: ' / settelements / view /: id' }]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettlementRoutingModule { }
