import { Component, OnInit } from '@angular/core';
import { StatService } from '../../../shared/services';
import { ToastyService } from 'ng2-toasty';
import { WithdrawFormComponent } from './../../withdraw-form/withdraw-form.component';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SettlementService } from "./../../service/settlement.service";

@Component({
  selector: 'settlement-listing',
  templateUrl: './listing.html',
  styleUrls: ['./listing.component.css'],
})
export class ListingComponent implements OnInit {
  public balance = 0;

  public items: any = {};

  public page: any = 1;
  public take: any = 5;
  public total: any = 0;
  public searchText: any = '';
  public sortOption = {
    sortBy: 'createdAt',
    sortType: 'desc'
  };


  constructor(
    private satttlementService: SettlementService,
    private toasty: ToastyService,
    private router: Router, private route: ActivatedRoute, private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.statQuery();
  }
  statQuery() {
    this.satttlementService.getBalance().then(resp => {
      // console.log(resp.data);
      this.balance = resp.data.balance;
    })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));


    this.satttlementService.getLogs({
      page: this.page,
      take: this.take,
      q: this.searchText,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`
    })
      .then(resp => {
        this.items = resp.data.items;
        this.total = resp.data.count;
        // console.log(resp);
      }
      )
      .catch(() => {
        this.toasty.error('Something went wrong, please try again!');
      });
  }

  query(){
    this.satttlementService.getLogs({
      page: this.page,
      take: this.take,
      q: this.searchText,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`
    })
      .then(resp => {
        this.items = resp.data.items;
        this.total = resp.data.count;
        // console.log(resp);
      }
      )
      .catch(() => {
        this.toasty.error('Something went wrong, please try again!');
      });
  }

  

  complain() {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false
    };
    let initialState = {
      balance: this.balance
    };
    this.modalService.open(WithdrawFormComponent, ngbModalOptions);
  }

}
