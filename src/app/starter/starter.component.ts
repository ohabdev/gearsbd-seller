import { Component, OnInit } from '@angular/core';
import { ShopService } from '../shop/shop.service';
import { ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

import Chart from 'chart.js';
import { MessageService } from '../message/services/message.service';
import { StatService } from '../shared/services';
@Component({
  templateUrl: './starter.component.html'
})
export class StarterComponent implements OnInit {

  public shop: any = {};
  public latestMessages: any = [];
  public orderStat: any = {};
  public revenueStat: any = {};
  public productStat: any = {};
  public saleStat: any = {};
  public notifications = {total: 0, data: []};

  private chartData: any = {
      'lebels'    : [],
      'sales'     : [],
      'views'     : []
  };
  private quickStatsData: object = {
    last24HoursOrders: 0,
    totalPendingOrders: 0,
    totalCancelledOrders: 0,
    totalRefundedOrders: 0,
    totalProducts: 0
  };

  public chartPostData : any = {
    "days" : 30
  };

  private chart: any;

  constructor(private shopService: ShopService, private statService: StatService, private toasty: ToastyService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.statQuery();

    this.shopService.me().then((res) => {
      this.shop = res.data;
    })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));

    this.messageService.latest({
      take: 4
    }).then(resp => this.latestMessages = resp.data.items);

    this.processChart();

  }
  dateFormat(date,option = {}) {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return monthNames[date.getMonth()]+' '+date.getDate();
  }

  processChart(){
    this.statService.chartData(this.chartPostData).then(resp => {
      var labels = [];
      var sales = [];
      var views = [];
      resp.data.items.forEach(item => {
        labels.push(this.dateFormat(new Date(item.day)));
        sales.push(parseFloat(item.totalSales).toFixed(2));
        //views.push(item.views);
      });
      this.chartData = {
          'lebels'    : labels,
          'sales'     : sales
          /*'views'     : views*/
      };
      this.drawChart();

    })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));
  }


  statQuery() {
    this.statService.orderStat().then(resp => {
      this.orderStat = resp.data;
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));

    this.statService.prodStat().then(resp => {
      this.productStat = resp.data;
    })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));

    this.statService.saleStat().then(resp => {
      this.saleStat = resp.data;
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));

    this.statService.revenueStat().then(resp => {
      const data = resp.data;
      if(data.yesterdaySales > 0){
        data.enhance =  ( ( data.todaySales - data.yesterdaySales) * 100 ) / data.yesterdaySales;
      }else{
        data.enhance = data.todaySales;
      }
      this.revenueStat = data;
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));

    this.statService.lastNotifications().then(res => {
      this.notifications = { total: res.data.total, data: res.data.notifications };
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));

  }

  drawChart(){
    var ctx = document.getElementById("linechart");
    // ctx.innerHTML = '';
    // console.log(ctx);
    if (ctx !== null) {
      this.chart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [
              {
                label: 'Sales',
                backgroundColor: "transparent",
                borderColor: "rgb(143, 138, 255)",
                data: this.chartData.sales,
                type: 'line'
            },
              {
                backgroundColor: "rgb(241, 242, 247)",
                // borderColor: "rgb(82, 136, 255)",
                label: 'Views',
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 2,
                data: this.chartData.views
            }

          ],
            labels: this.chartData.lebels
        },
      });
    }
  }
  changeDays(){
    if(this.chart){
      this.chart.destroy();
      this.processChart();
    }
  };
}
